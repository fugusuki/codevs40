﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

abstract class Strategy
{
    protected List<Unit> units = new List<Unit>();
    public readonly ConcurrentStack<Tuple<Unit, UnitType, int>> RequestForBuilding;
    protected void Request(Unit unit, UnitType unitType, int priority)
    {
        this.RequestForBuilding.Push(Tuple.Create(unit, unitType, priority));
    }

    public readonly ConcurrentStack<Tuple<Unit, Type>> ConvertList;
    protected void Convert<TStrategy>(Unit unit) where TStrategy : Strategy
    {
        this.ConvertList.Push(Tuple.Create(unit, typeof(TStrategy)));
    }

    public Strategy()
    {
        this.RequestForBuilding = new ConcurrentStack<Tuple<Unit, UnitType, int>>();
        this.ConvertList = new ConcurrentStack<Tuple<Unit, Type>>();
    }

    public IDictionary<Unit, char> GetActions(Condition condition, List<Unit> builtUnits, List<Unit> convertedUnits, List<Unit> killedUnits)
    {
        if (condition.currentInput.Turn == 0) this.Initialize(condition);

        this.RequestForBuilding.Clear();
        foreach (var conv in this.ConvertList) this.units.Remove(conv.Item1);
        this.ConvertList.Clear();
        this.units.AddRange(builtUnits);
        this.units.AddRange(convertedUnits);
        foreach (var killed in killedUnits) this.units.RemoveAll(u => u.Id == killed.Id);

        Think(condition, builtUnits, convertedUnits, killedUnits);

        return this.units.Where(u => u.TargetPoint != null).ToDictionary(u => u, u => this.Move(u, condition));
    }

    protected virtual void Initialize(Condition condition) { }

    protected abstract void Think(Condition condition, List<Unit> builtUnits, List<Unit> convertedUnits, List<Unit> killedUnits);

    protected virtual char Move(Unit unit, Condition condition)
    {

        var nextPoint = unit.Point.GetPointsByDistanceInside(1).OrderByDescending(p => GetEvalFuncMove(condition, unit, p)).ThenBy(p => unit.TargetPoint.DistanceFrom(p)).First();
        return nextPoint.DirectionFrom(unit.Point);
    }

    protected int GetEvalFuncMove(Condition condition, Unit unit, Point targetPoint)
    {
        var unitCount = new int[Unit.DamageArrayAttack.Length];
        var sum = 0;
        foreach (var p in targetPoint.GetPointsByDistanceInside(2))
        {
            var units = condition.unitMap[p.Y][p.X];
            sum += Math.Min(10, units.Count);
            foreach (var u in units) unitCount[(int)u.Type]++;
        }

        var enemyAttack = condition.enemyAttackMap[targetPoint.Y][targetPoint.X].Zip(unitCount, (a, c) => a * c).Sum() / sum;
        var attack = condition.enemyDefenceMap[targetPoint.Y][targetPoint.X].Zip(unitCount, (d, c) => d * c).Sum();
        var forward = (unit.TargetPoint ?? unit.Point).DirectionFrom(unit.Point);
        var forwardPoint = unit.Point.GetNextPoint(forward);
        return this.EvalFuncMove(enemyAttack, attack, condition, unit, forwardPoint, targetPoint);
    }

    protected virtual int EvalFuncMove(int enemyAttack, int attack, Condition condition, Unit unit, Point forwardPoint, Point p)
    {
        return -p.DistanceFrom(forwardPoint);
    }
}

class Strategies
{
    public static IEnumerable<Strategy> AllStrategies
    {
        get
        {
            return typeof(Strategies).GetNestedTypes().Select(t => t.GetConstructor(new Type[] { }).Invoke(new object[] { })).Cast<Strategy>();
        }
    }

    public class GuardCastleStrategy : Strategy
    {
        protected override void Think(Condition condition, List<Unit> builtUnits, List<Unit> convertedUnits, List<Unit> killedUnits)
        {
            var enemyCount = Utils.GetUnitsInside(condition.enemyUnitMap, condition.castle.Point, 10).Count();
            var buildCount = enemyCount;
            if (buildCount > 0)
            {
                var workers = this.units.Where(u => u.Type == UnitType.Worker);
                if (workers.Any())
                {
                    foreach (var w in workers)
                    {
                        if (Utils.GetUnitsInside(condition.unitMap, w.Point, 0).Count() < Math.Min(10, buildCount)) this.Request(w, UnitType.Village, 999999);
                    }
                }
                else
                {
                    this.Request(condition.castle, UnitType.Worker, 99999);
                }
            }
        }
    }

    public class SearchStrategy : Strategy
    {
        private readonly Dictionary<Unit, Queue<Point>> assigned = new Dictionary<Unit, Queue<Point>>();
        private readonly Queue<Point[]> targetPoints = new Queue<Point[]>();
        private readonly Random random = new Random(1);

        protected override int EvalFuncMove(int enemyAttack, int attack, Condition condition, Unit unit, Point forwardPoint, Point p)
        {
            if (!condition.needsRapidAttack && condition.enemyCastle != null && condition.enemyCastle.DistanceFrom(p) <= 15) return -1000000 * (20 - condition.enemyCastle.DistanceFrom(p));
            return -enemyAttack + (p == forwardPoint ? 120 : 0) - (p.DirectionFrom(unit.Point) == unit.Point.DirectionFrom(unit.PrevPoint) ? 1 : 0) - (((p == unit.Point || p == unit.PrevPoint) && p != unit.TargetPoint) ? 5000 : 0);
        }

        protected override void Initialize(Condition condition)
        {
            var ny = (condition.castle.Y - 4) / 9;
            var nx = (condition.castle.X - 4) / 9;

            //横縦横縦横
            this.targetPoints.Enqueue(new Point[] { Point.Get(ny * 9 + 4, 99 - (ny * 9)), Point.Get(99 - 59, 99 - (ny * 9)), Point.Get(99 - 59, 99 - 2), Point.Get(99, 99 - 2) });
            this.targetPoints.Enqueue(new Point[] { Point.Get(ny * 9 + 13, 99 - (ny * 9 + 9)), Point.Get(99 - 50, 99 - (ny * 9 + 9)), Point.Get(99 - 50, 99 - 11), Point.Get(99, 99 - 11) });

            //縦横縦横縦
            this.targetPoints.Enqueue(new Point[] { Point.Get(99 - (nx * 9), nx * 9 + 4), Point.Get(99 - (nx * 9), 99 - 59), Point.Get(99 - 2, 99 - 59), Point.Get(99 - 2, 99) });
            this.targetPoints.Enqueue(new Point[] { Point.Get(99 - (nx * 9 + 9), nx * 9 + 13), Point.Get(99 - (nx * 9 + 9), 99 - 50), Point.Get(99 - 11, 99 - 50), Point.Get(99 - 11, 99) });

            for (int n = 0; this.targetPoints.Count < 8; n++)
            {
                if (n < ny || ny + 1 < n) this.targetPoints.Enqueue(new Point[] { Point.Get(n * 9 + 4, 99 - (n * 9)), Point.Get(99, 99 - (n * 9)) });
                if (n < nx || nx + 1 < n) this.targetPoints.Enqueue(new Point[] { Point.Get(99 - (n * 9), n * 9 + 4), Point.Get(99 - (n * 9), 99) });
            }
        }

        protected override void Think(Condition condition, List<Unit> builtUnits, List<Unit> convertedUnits, List<Unit> killedUnits)
        {
            if (killedUnits.Any())
            {
                for (int i = this.units.Count; i < 2; i++)
                {
                    if (this.units.Count == 0) this.targetPoints.Enqueue(new Point[] { condition.castlePossiblePoints.ElementAt(random.Next(condition.castlePossiblePoints.Count)) });
                }
            }

            for (int i = 0; i < this.units.Count; i++)
            {
                var unit = this.units[i];
                if (unit.TargetPoint == null)
                {
                    if (this.targetPoints.Count > 0)
                        this.assigned.Add(this.units[i], new Queue<Point>(this.targetPoints.Dequeue()));
                }

                if (unit.TargetPoint == null || (this.units[i].Point.Y >= unit.TargetPoint.Y && this.units[i].Point.X >= unit.TargetPoint.X && this.units[i].Point.DistanceFrom(condition.castle.Point) >= 10))
                {
                    Queue<Point> a;
                    this.assigned.TryGetValue(unit, out a);
                    if (a != null)
                    {
                        if (a.Count > 0) unit.TargetPoint = a.Dequeue();
                        else
                        {
                            if (condition.enemyCastle != null && condition.builders.Any(b => b.Type == UnitType.Base))
                            {
                                var targets = condition.enemyCastle.GetPointsByDistance(15).ToList();
                                unit.TargetPoint = targets.ElementAt(random.Next(targets.Count));
                            }
                            else
                            {
                                unit.TargetPoint = condition.castlePossiblePoints.ElementAt(random.Next(condition.castlePossiblePoints.Count));
                            }
                        }
                    }
                }
            }

            if (condition.enemyCastle == null)
            {
                foreach (var p in condition.castleDamagePoints)
                {
                    var unit = this.units.FirstOrDefault(u => u.Point == p);
                    if (unit != null)
                    {
                        //Console.Error.WriteLine (condition.currentInput.Turn + " " + p.Y + " " + p.X);
                        this.Request(unit, UnitType.Village, 100000);
                    }
                }
            }

            if (condition.needsRapidAttack || condition.needsNoBuild)
            {
                if (condition.enemyCastle != null)
                {
                    var target = condition.enemyCastle;
                    if (this.units.Any())
                    {
                        if (!condition.builders.Any(b => b.Type == UnitType.Base))
                        {
                            var builder = this.units.OrderBy(u => u.Point.DistanceFrom(target)).FirstOrDefault();
                            if (condition.currentInput.currentResources > 600) this.Request(builder, UnitType.Base, 1001);
                        }
                        else if (condition.builders.Count(b => b.Type == UnitType.Base) < 2)
                        {
                            var builder = this.units.OrderBy(u => u.Point.DistanceFrom(condition.builders.First(b => b.Type == UnitType.Base).Point)).First();
                            if (condition.currentInput.currentResources > 600 && condition.income > 30) this.Request(builder, UnitType.Base, 1001);
                        }
                        //if (condition.currentInput.currentResources > 600) this.Request(builder, UnitType.Base, 1001);
                    }
                    for (int i = 0; i < this.units.Count; i++) this.units[i].TargetPoint = target;
                }
                else if (!condition.builders.Any(b => b.Type == UnitType.Base))
                {
                    this.Request(null, UnitType.Base, 1000);
                    this.Request(null, UnitType.Base, 1000);
                    this.Request(null, UnitType.Base, 1000);
                    this.Request(null, UnitType.Base, 1000);
                    this.Request(null, UnitType.Base, 1000);
                    this.Request(null, UnitType.Base, 1000);
                }
            }
            else
            {
                if (condition.currentInput.currentResources >= 500 && condition.currentInput.Turn > 120)
                {
                    if (!condition.builders.Any(b => b.Type == UnitType.Base) || (condition.currentInput.currentResources > 1000))
                    {
                        var enemyCastle = condition.enemyCastle ?? Utils.GetAveragePoint(condition.castlePossiblePoints);

                        var unit = this.units.Where(u => (u.Y >= enemyCastle.Y || u.X >= enemyCastle.X) && u.Point.DistanceFrom(enemyCastle) >= 15).OrderBy(u => u.Point.DistanceFrom(enemyCastle)).FirstOrDefault();
                        //if (unit != null && unit.Point.DistanceFrom(Point.Get(99,99)) < 100)
                        if (unit != null)
                        {
                            if (condition.currentInput.currentResources > 1000)
                            {
                                this.Request(unit, UnitType.Base, 99);
                            }
                        }
                    }
                }

                if (!condition.needsNoBuild)
                {
                    foreach (var resource in condition.resources.Where(resource => condition.builders.Where(builder => builder.Type != UnitType.Base).All(b => resource.DistanceFrom(b.Point) > 30)))
                    {
                        int mind = 10;
                        Unit minu = null;
                        foreach (var unit in this.units)
                        {
                            var d = resource.DistanceFrom(unit.Point);
                            if (d < mind)
                            {
                                mind = d;
                                minu = unit;
                            }
                        }
                        if (minu != null)
                        {
                            if (!Utils.GetUnitsInside(condition.enemyUnitMap, resource, 4).Any() && Utils.GetUnitsInside(condition.unitMap, resource, 4).Any())
                                if (condition.currentInput.Turn < 150) this.Request(minu, UnitType.Village, 100);
                        }
                    }
                }
            }

            if (this.targetPoints.Any())
            {
                if (condition.currentInput.Turn < 100 && condition.currentInput.currentResources < 200) { }
                else if (condition.enemyCastle != null && condition.builders.Any(b => b.Type == UnitType.Base)) { }
                else
                {
                    var target = this.targetPoints.Peek().First();
                    Unit builder = this.targetPoints.Peek().Length > 1 ? condition.castle : condition.builders.Where(b => b.Type == UnitType.Village || b.Type == UnitType.Castle).OrderBy(b => b.Point.DistanceFrom(target)).First();
                    var priority = condition.builders.Any(b => b.Type == UnitType.Base) ? 20 : 200;
                    this.Request(builder, UnitType.Worker, priority);
                }
            }
        }
    }

    public class ResourceStrategy : Strategy
    {
        List<Point> resources = new List<Point>();
        List<Point> targetPointsWorker = new List<Point>();
        List<Point> targetPointsWarrior = new List<Point>();

        protected override int EvalFuncMove(int enemyAttack, int attack, Condition condition, Unit unit, Point forwardPoint, Point p)
        {
            if (p == unit.TargetPoint && enemyAttack < 100) return 1000;
            return forwardPoint == p ? 1000 : 0;
        }

        protected override void Think(Condition condition, List<Unit> builtUnits, List<Unit> convertedUnits, List<Unit> killedUnits)
        {
            if (this.resources.Count < condition.resources.Count)
            {
                var newResources = condition.resources.Except(this.resources).ToList();
                foreach (var p in newResources)
                {
                    if (condition.enemyUnitMap[p.Y][p.X].Count <= 2) targetPointsWorker.AddRange(Enumerable.Repeat(p, 5));
                    else targetPointsWarrior.AddRange(Enumerable.Repeat(p, 3));
                }
                this.resources.AddRange(newResources);
            }

            for (int i = 0; i < this.units.Count; i++)
            {
                if (this.units[i].TargetPoint == null)
                {
                    if (this.units[i].Type == UnitType.Worker)
                    {
                        int mind = 30;
                        int minj = -1;
                        for (int j = 0; j < targetPointsWorker.Count; j++)
                        {
                            int d = this.units[i].Point.DistanceFrom(this.targetPointsWorker[j]);
                            if (d < mind)
                            {
                                mind = d;
                                minj = j;
                            }
                        }
                        if (minj >= 0)
                        {
                            this.units[i].TargetPoint = this.targetPointsWorker[minj];
                            this.targetPointsWorker.RemoveAt(minj);
                        }
                    }
                    else
                    {
                        this.units[i].TargetPoint = this.targetPointsWarrior[0];
                        this.targetPointsWarrior.RemoveAt(0);
                    }
                }
                else if (this.units[i].TargetPoint == this.units[i].Point)
                {
                    if (this.units[i].Type != UnitType.Worker && this.units.Count(u => u.TargetPoint == this.units[i].TargetPoint && u.Type != UnitType.Worker) > 2) this.Convert<SurpriseAttackStrategy>(this.units[i]);
                }
            }

            if (!condition.needsRapidAttack && !condition.needsNoBuild)
            {
                foreach (var p in this.targetPointsWorker)
                {
                    int mind = 30;
                    Unit minb = null;
                    foreach (var builder in condition.builders.Where(b => b.Type != UnitType.Base))
                    {
                        int d = p.DistanceFrom(builder.Point);
                        if (d < mind)
                        {
                            mind = d;
                            minb = builder;
                        }
                    }
                    if (minb != null && condition.currentInput.currentResources > 150)
                    {
                        this.Request(minb, UnitType.Worker, 80 - mind);
                    }
                }
                //foreach (var p in this.targetPointsWarrior)
                //{
                //    int mind = int.MaxValue;
                //    Unit minb = null;
                //    foreach (var builder in condition.builders.Where(b => b.Type == UnitType.Base))
                //    {
                //        int d = p.DistanceFrom(builder.Point);
                //        if (d < mind)
                //        {
                //            mind = d;
                //            minb = builder;
                //        }
                //    }
                //    if (minb != null)
                //    {
                //        this.Request(minb, UnitType.Assassin, 65);
                //    }
                //}
            }
            else
            {
                foreach (var u in this.units.Where(u => u.Type != UnitType.Worker)) this.Convert<RapidAttackStrategy>(u);
            }
        }
    }

    public class RapidAttackStrategy : Strategy
    {
        Random random = new Random(0);

        protected override int EvalFuncMove(int enemyAttack, int attack, Condition condition, Unit unit, Point forwardPoint, Point p)
        {
            return -100 * unit.TargetPoint.DistanceFrom(p) + (condition.enemyCastle == null ? 0 : -condition.enemyCastle.DistanceFrom(p) + p.X - condition.enemyCastle.X + p.Y - condition.enemyCastle.Y);
        }

        Point target = null;
        Point firstBasePoint = null;
        protected override void Think(Condition condition, List<Unit> builtUnits, List<Unit> convertedUnits, List<Unit> killedUnits)
        {
            if (!condition.needsRapidAttack) return;

            var c = 0;
            Unit firstBase = condition.builders.FirstOrDefault(b => b.Type == UnitType.Base);
            if (firstBase != null) firstBasePoint = firstBase.Point;

            Point currentTarget;
            if (condition.enemyCastle != null && firstBasePoint != null)
            {
                target = target ?? condition.enemyCastle.GetPointsByDistance(2).OrderBy(p => p.DistanceFrom(firstBasePoint)).Take(3).OrderBy(p => condition.enemyCountMap[p.Y][p.X].Sum()).ThenBy(p => p.DistanceFrom(condition.enemyCastle)).First();
                currentTarget = target;

                var currentTarget2 = currentTarget;
                while (condition.enemyUnitMap[currentTarget.Y][currentTarget.X].Count > 2)
                {
                    currentTarget = currentTarget.GetNextPoint(currentTarget.DirectionFrom(condition.enemyCastle));
                    if (currentTarget == currentTarget2) break;
                    currentTarget2 = currentTarget;
                }
                if (Utils.GetUnitsInside(condition.unitMap, currentTarget, 1).Count() < Utils.GetUnitsInside(condition.enemyUnitMap, currentTarget, 2).Count())
                {
                    currentTarget = currentTarget.GetNextPoint(currentTarget.DirectionFrom(condition.enemyCastle));
                }
                if (Utils.GetUnitsInside(condition.unitMap, currentTarget, 1).Count() < Utils.GetUnitsInside(condition.enemyUnitMap, currentTarget, 2).Count())
                {
                    currentTarget = currentTarget.GetNextPoint(currentTarget.DirectionFrom(condition.enemyCastle));
                }

                var count = 0;
                foreach (var unit in this.units.OrderBy(u => currentTarget.DistanceFrom(u.Point)))
                {
                    unit.TargetPoint = currentTarget;
                    if (condition.enemyUnitMap[condition.enemyCastle.Y][condition.enemyCastle.X].Count > 5 && count++ == 30)
                    {
                        currentTarget = currentTarget.GetNextPoint(currentTarget.DirectionFrom(condition.enemyCastle));
                        count = 0;
                    }
                }
            }
            else
            {
                currentTarget = Utils.GetAveragePoint(condition.castlePossiblePoints);
                for (int i = 0; i < this.units.Count; i++)
                {
                    if (this.units[i].TargetPoint == null || this.units[i].TargetPoint == this.units[i].Point) this.units[i].TargetPoint = condition.castlePossiblePoints.ElementAt(random.Next(condition.castlePossiblePoints.Count));
                }
            }

            foreach (var b in condition.builders.OrderBy(b => (b.Type == UnitType.Base ? 1 : 2) * b.Point.DistanceFrom(currentTarget)))
            {
                if (b.Type == UnitType.Base)
                {
                    if (condition.builders.Count(bb => bb.Type == UnitType.Base) >= 2 && condition.currentInput.currentResources > 140) this.Request(b, UnitType.Assassin, 999 - c++);
                    else this.Request(b, UnitType.Knight, 999 - c++);
                }
                //else if (b.Point.DistanceFrom(target) <= 10 && condition.unitMap[condition.castle.Y][condition.castle.X].Count >= 8) this.Request(b, UnitType.Worker, 999 - c++);
            }
        }
    }

    public class SurpriseAttackStrategy : Strategy
    {
        Random random = new Random(0);
        bool dispatched = false;
        List<Unit>[][] assignedMap;
        const int AssignedMapSize = 10;

        protected override int EvalFuncMove(int enemyAttack, int attack, Condition condition, Unit unit, Point forwardPoint, Point p)
        {
            return -100 * unit.TargetPoint.DistanceFrom(p) + (condition.enemyCastle == null ? 0 : -condition.enemyCastle.DistanceFrom(p) + p.X - condition.enemyCastle.X + p.Y - condition.enemyCastle.Y);
        }

        protected override void Initialize(Condition condition)
        {
            assignedMap = Enumerable.Range(0, AssignedMapSize * 2 + 1).Select(y => Enumerable.Range(0, AssignedMapSize * 2 + 1).Select(x => new List<Unit>()).ToArray()).ToArray();
        }

        protected override void Think(Condition condition, List<Unit> builtUnits, List<Unit> convertedUnits, List<Unit> killedUnits)
        {
            if (condition.needsRapidAttack)
            {
                this.units.ForEach(u => this.Convert<RapidAttackStrategy>(u));
                return;
            }

            foreach (var k in killedUnits)
            {
                foreach (var p in Point.Get(AssignedMapSize, AssignedMapSize).GetPointsByDistanceInside(AssignedMapSize))
                {
                    if (assignedMap[p.Y][p.X].Remove(k)) break;
                }
            }
            foreach (var p in Point.Get(AssignedMapSize, AssignedMapSize).GetPointsByDistanceInside(AssignedMapSize))
            {
                if (assignedMap[p.Y][p.X].Count < 10)
                {
                    foreach (var pp in p.GetPointsByDistanceInside(1))
                    {
                        if (assignedMap[p.Y][p.X].Count >= 10) break;
                        if (pp.Y < 0 || pp.X < 0 || assignedMap.Length <= pp.Y || assignedMap[0].Length <= pp.X) continue;
                        if (Point.Get(AssignedMapSize, AssignedMapSize).DistanceFrom(pp) <= Point.Get(AssignedMapSize, AssignedMapSize).DistanceFrom(p)) continue;

                        while (assignedMap[pp.Y][pp.X].Any())
                        {
                            assignedMap[p.Y][p.X].Add(assignedMap[pp.Y][pp.X].Last());
                            assignedMap[pp.Y][pp.X].RemoveAt(assignedMap[pp.Y][pp.X].Count - 1);
                            if (assignedMap[p.Y][p.X].Count >= 10) break;
                        }
                    }
                }
            }

            foreach (var b in condition.builders.Where(b => b.Type == UnitType.Base))
            {
                var unitCount = new int[7];
                foreach (var eu in Utils.GetUnitsInside(condition.enemyUnitMap, condition.enemyCastle != null ? condition.enemyCastle : Point.Get(99, 99), condition.enemyCastle != null ? 20 : 40))
                {
                    unitCount[(int)eu.Type]++;
                }
                var priority = 60;
                if (condition.currentInput.currentResources >= 0)
                {
                    if (unitCount[(int)UnitType.Knight] > unitCount[(int)UnitType.Assassin])
                        this.Request(b, random.Next(3) == 0 ? UnitType.Assassin : UnitType.Fighter, priority);
                    else if (condition.currentInput.currentResources < 40 && unitCount[(int)UnitType.Assassin] > unitCount[(int)UnitType.Knight] + unitCount[(int)UnitType.Fighter])
                        this.Request(b, random.Next(3) == 0 ? UnitType.Assassin : UnitType.Knight, priority);
                    else
                    {
                        this.Request(b, random.Next(3) == 0 ? UnitType.Fighter : UnitType.Assassin, priority);
                    }
                }
            }


            var thTurn = 200;
            if (condition.enemyCastle != null && Utils.GetUnitsInside(condition.enemyUnitMap, condition.enemyCastle, 1).Count() >= 10) thTurn = 245;

            if (condition.currentInput.Turn > thTurn || Utils.GetUnitsInside(condition.enemyUnitMap, condition.castle.Point, 10).Count() > 5)
            {
                var target = condition.enemyCastle ?? condition.castlePossiblePoints.ElementAt(random.Next(condition.castlePossiblePoints.Count));
                if (assignedMap[AssignedMapSize][AssignedMapSize].Any()) target = target.GetPointsByDistance(1).OrderBy(p => assignedMap[AssignedMapSize][AssignedMapSize].First().Point.DistanceFrom(p)).First();

                if (!dispatched)
                {
                    for (int i = 0; i < this.units.Count; i++)
                    {
                        var unit = this.units[i];
                        foreach (var p in Point.Get(AssignedMapSize, AssignedMapSize).GetPointsByDistanceInside(AssignedMapSize))
                        {
                            if (assignedMap[p.Y][p.X].Contains(unit))
                            {
                                unit.TargetPoint = target.Offset(p.Y - AssignedMapSize, p.X - AssignedMapSize);
                                break;
                            }
                        }
                    }
                    dispatched = true;
                }
                else
                {
                    for (int i = 0; i < this.units.Count; i++)
                    {
                        var unit = this.units[i];
                        if (unit.TargetPoint == null)
                        {
                            var a = Point.Get(AssignedMapSize, AssignedMapSize).GetPointsByDistanceInside(1).FirstOrDefault(p => assignedMap[p.Y][p.X].Count < 10) ?? Point.Get(AssignedMapSize, AssignedMapSize);
                            unit.TargetPoint = target.Offset(a.Y - AssignedMapSize, a.X - AssignedMapSize);
                            assignedMap[AssignedMapSize + unit.TargetPoint.Y - target.Y][AssignedMapSize + unit.TargetPoint.X - target.X].Add(unit);
                        }
                        else if (unit.TargetPoint == unit.Point)
                        {
                            foreach (var p in Point.Get(AssignedMapSize, AssignedMapSize).GetPointsByDistanceInside(AssignedMapSize))
                            {
                                if (assignedMap[p.Y][p.X].Contains(unit))
                                {
                                    unit.TargetPoint = target.Offset(p.Y - AssignedMapSize, p.X - AssignedMapSize);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < this.units.Count; i++)
                {
                    var unit = this.units[i];
                    if (unit.TargetPoint == null)
                    {
                        unit.TargetPoint = Point.Get(AssignedMapSize, AssignedMapSize).GetPointsByDistanceInside(AssignedMapSize).Where(p => 0 <= p.Y + unit.Y - AssignedMapSize && p.Y + unit.Y - AssignedMapSize < 100 && 0 <= p.X + unit.X - AssignedMapSize && p.X + unit.X - AssignedMapSize < 100).First(p => assignedMap[p.Y][p.X].Count < 10).Offset(unit.Y - AssignedMapSize, unit.X - AssignedMapSize);
                        assignedMap[AssignedMapSize + unit.TargetPoint.Y - unit.Point.Y][AssignedMapSize + unit.TargetPoint.X - unit.Point.X].Add(unit);
                    }
                }
            }
        }
    }
}