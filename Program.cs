﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("testAI");
        Solver solver = null;
        while (true)
        {
            Input input = Input.FromConsole();
            if (input.Turn == 0) solver = new Solver(input);
            var output = solver.NextOutput(input).Where(kv => kv.Value != ' ').Select(kv => kv.Key.Id + " " + (input.is1P ? kv.Value : (kv.Value == 'U' ? 'D' : kv.Value == 'D' ? 'U' : kv.Value == 'R' ? 'L' : kv.Value == 'L' ? 'R' : kv.Value))).ToList();
            Console.WriteLine(output.Count);
            output.ForEach(Console.WriteLine);
        }

    }
}

class Condition
{
    public Point[][] points;
    public List<Unit>[][] prevUnitMap;
    public List<Unit>[][] unitMap;
    public List<Unit>[][] enemyUnitMap;
    public List<Unit>[][] prevEnemyUnitMap;
    public int[][][] enemyAttackMap;  //y, x, UnitType 敵の攻撃 合計値
    public int[][][] enemyDefenceMap;  //y, x, UnitType 味方の攻撃 一人当たり
    public int[][][] enemyCountMap;   //y, x, UnitType 敵の人数 一人当たり
    public Unit castle;
    public List<Point> resources;
    public Input currentInput = null;
    public Input prevInput = null;
    public List<Unit> units = new List<Unit>();
    public List<Unit> builders = new List<Unit>();

    public Point enemyCastle = null;
    public int[][] castleSearchMap;
    public List<Point> castleDamagePoints = new List<Point>();
    public HashSet<Point> castlePossiblePoints;

    public bool needsRapidAttack = false;
    public bool needsNoBuild = false;
    public int buildCount = 0;
    public int enemyBuildCount = 0;
    public int income = 0;
}

class Solver
{
    Condition condition;
    Strategy[] strategies = Strategies.AllStrategies.ToArray();


    public Solver(Input initial)
    {
        this.condition = new Condition();
        this.condition.points = Enumerable.Range(0, 100).Select(y => Enumerable.Range(0, 100).Select(x => Point.Get(y, x)).ToArray()).ToArray();
        this.condition.resources = new List<Point>();
        this.condition.unitMap = Enumerable.Range(0, 100).Select(y => Enumerable.Range(0, 100).Select(x => new List<Unit>()).ToArray()).ToArray();
        this.condition.prevUnitMap = Enumerable.Range(0, 100).Select(y => Enumerable.Range(0, 100).Select(x => new List<Unit>()).ToArray()).ToArray();
        this.condition.enemyUnitMap = Enumerable.Range(0, 100).Select(y => Enumerable.Range(0, 100).Select(x => new List<Unit>()).ToArray()).ToArray();
        this.condition.prevEnemyUnitMap = Enumerable.Range(0, 100).Select(y => Enumerable.Range(0, 100).Select(x => new List<Unit>()).ToArray()).ToArray();
        this.condition.castlePossiblePoints = new HashSet<Point>(Point.Get(99, 99).GetPointsByDistanceInside(40));
        this.condition.castleSearchMap = Enumerable.Range(0, 100).Select(y => new int[100]).ToArray();
        foreach (var p in Point.Get(99, 99).GetPointsByDistanceInside(50)) this.condition.castleSearchMap[p.Y][p.X] = -1;

        if (initial.Turn != 0) throw new ArgumentException();
        condition.castle = initial.Units.Where(u => u.Type == UnitType.Castle).Single();

        this.requestsForBuilding = Enumerable.Repeat(strategies.Single(st => st is Strategies.SearchStrategy), 5).Select(st => Tuple.Create(st, this.condition.castle, UnitType.Worker, 0)).ToList();
    }


    List<Tuple<Strategy, Unit, UnitType, int>> requestsForBuilding = new List<Tuple<Strategy, Unit, UnitType, int>>();
    public IDictionary<Unit, char> NextOutput(Input input)
    {
        this.condition.prevInput = this.condition.currentInput;
        this.condition.currentInput = input;

        for (int y = 0; y < this.condition.unitMap.Length; y++)
        {
            for (int x = 0; x < this.condition.unitMap[y].Length; x++)
            {
                this.condition.prevUnitMap[y][x].Clear();
                this.condition.prevUnitMap[y][x].AddRange(this.condition.unitMap[y][x]);
                this.condition.unitMap[y][x].Clear();
                this.condition.prevEnemyUnitMap[y][x].Clear();
                this.condition.prevEnemyUnitMap[y][x].AddRange(this.condition.enemyUnitMap[y][x]);
                this.condition.enemyUnitMap[y][x].Clear();
            }
        }
        foreach (var enemyUnit in this.condition.currentInput.EnemyUnits)
        {
            this.condition.enemyUnitMap[enemyUnit.Y][enemyUnit.X].Add(enemyUnit);

            if (!condition.needsNoBuild)
            {
                if (input.Turn < 150 && enemyUnit.Type != UnitType.Worker && enemyUnit.Type != UnitType.Village && enemyUnit.Point.DistanceFrom(Point.Get(0, 0)) < 60)
                    this.condition.needsNoBuild = true;
            }
        }

        var enemyCastleUnit = this.condition.currentInput.EnemyUnits.SingleOrDefault(u => u.Type == UnitType.Castle);
        if (enemyCastleUnit != null)
        {
            this.condition.enemyCastle = enemyCastleUnit.Point;

            if (condition.enemyUnitMap[condition.enemyCastle.Y][condition.enemyCastle.X].Any(u => u.Type == UnitType.Castle))
            {
                //colunAI, chokudAI, silber, zinnoberは速攻
                //grun, schwartz, lilaは溜めて奇襲
                this.condition.needsRapidAttack = !condition.enemyUnitMap[condition.enemyCastle.Y][condition.enemyCastle.X].Any(u => u.Type == UnitType.Base);
            }
        }

        this.condition.enemyCountMap = Utils.GetInfluenceMap(this.condition.enemyUnitMap, 3, (u, units, distance, type) => u.Type == type ? (distance == 3 ? 1 : distance == 2 ? 3 : 4) : 0);
        this.condition.enemyAttackMap = Utils.GetInfluenceMap(this.condition.enemyUnitMap, 3, (u, units, distance, type) => Unit.DamageArrayAttack[(int)u.Type][(int)type] * (distance == 3 ? 1 : distance == 2 ? 3 : 4) / 4);
        this.condition.enemyDefenceMap = Utils.GetInfluenceMap(this.condition.enemyUnitMap, 3, (u, units, distance, type) => Unit.DamageArrayAttack[(int)type].Zip(this.condition.enemyCountMap[u.Y][u.X], (a, c) => a * c * (distance == 3 ? 1 : distance == 2 ? 3 : 4) / 4).Sum() / u.Point.GetPointsByDistanceInside(3).Sum(p => Math.Min(this.condition.enemyUnitMap[p.Y][p.X].Count, 10) * (distance == 3 ? 1 : distance == 2 ? 3 : 4)) / this.condition.enemyCountMap[u.Y][u.X].Sum());

        this.condition.resources = this.condition.resources.Union(this.condition.currentInput.Resources).ToList();

        var killed = new List<Unit>();
        var built = new List<Unit>();
        {
            var checkCastleDamage = new List<Point>();
            int j = 0;
            for (int i = 0; i < this.condition.units.Count; i++)
            {
                var unit = this.condition.units[i];
                if (j < this.condition.currentInput.Units.Count && unit.Id == this.condition.currentInput.Units[j].Id)
                {
                    var inputUnit = this.condition.currentInput.Units[j];
                    unit.Point = inputUnit.Point;
                    unit.HPDiff = unit.HP - inputUnit.HP;
                    unit.HP -= unit.HPDiff;

                    var currentPoint = unit.Point;
                    if (this.condition.enemyCastle == null)
                    {
                        if (this.condition.castleSearchMap[currentPoint.Y][currentPoint.X] == -1)
                        {
                            foreach (var p in currentPoint.GetPointsByDistanceInside(4)) this.condition.castlePossiblePoints.Remove(p);
                        }
                        if (this.condition.castleSearchMap[currentPoint.Y][currentPoint.X] != 0 && this.condition.castleSearchMap[currentPoint.Y][currentPoint.X] != this.condition.currentInput.Turn)
                        {
                            checkCastleDamage.Add(currentPoint);
                            this.condition.castleSearchMap[currentPoint.Y][currentPoint.X] = this.condition.currentInput.Turn;
                        }
                    }
                    this.condition.unitMap[unit.Y][unit.X].Add(unit);

                    j++;
                }
                else
                {
                    unit.HPDiff = unit.HP;
                    unit.HP = 0;
                }
            }
            for (; j < this.condition.currentInput.Units.Count; j++)
            {
                var unit = this.condition.currentInput.Units[j];

                this.condition.unitMap[unit.Y][unit.X].Add(unit);

                if (unit.Type == UnitType.Castle || unit.Type == UnitType.Village || unit.Type == UnitType.Base) this.condition.builders.Add(unit);
                else built.Add(unit);
                this.condition.units.Add(unit);

                this.condition.buildCount++;
                this.condition.enemyBuildCount = unit.Id - this.condition.buildCount;
            }

            checkCastleDamage.ForEach(cp =>
            {
                var prevUnits = Utils.GetUnitsInside(this.condition.prevUnitMap, cp, 3).ToList();
                var currentUnits = Utils.GetUnitsInside(this.condition.unitMap, cp, 4).ToList();
                var prevEnemy = Utils.GetUnitsInside(this.condition.prevEnemyUnitMap, cp, 3).ToList();
                var currentEnemy = Utils.GetUnitsInside(this.condition.enemyUnitMap, cp, 4).ToList();
                if (prevEnemy.All(pe => currentEnemy.Any(ce => pe.Id == ce.Id)) && prevUnits.All(pu => currentUnits.Any(cu => pu.Id == cu.Id)))
                {
                    var insideUnitCount = Utils.GetUnitsInside(this.condition.unitMap, cp, 2).Count();
                    var insideEnemy = Utils.GetUnitsInside(this.condition.enemyUnitMap, cp, 2).ToList();

                    if (insideEnemy.Count == 0)
                    {
                        if (this.condition.unitMap[cp.Y][cp.X][0].HPDiff == 0)
                        {
                            foreach (var p in cp.GetPointsByDistanceInside(10)) this.condition.castlePossiblePoints.Remove(p);
                        }
                        else
                        {
                            this.condition.castleDamagePoints.Add(cp);
                            this.condition.castlePossiblePoints.RemoveWhere(p => cp.DistanceFrom(p) > 10);
                        }
                        this.condition.castleSearchMap[cp.Y][cp.X] = 0;
                    }
                }
            });
        }
        for (int i = this.condition.units.Count - 1; i >= 0; i--)
        {
            var unit = this.condition.units[i];
            if (unit.HP == 0)
            {
                this.condition.units.RemoveAt(i);
                killed.Add(unit);
                if (unit.Type == UnitType.Village || unit.Type == UnitType.Base) this.condition.builders.RemoveAll(b => b.Id == unit.Id);
            }
        }

        var builtRequests = this.requestsForBuilding.Select(req =>
        {
            var idx = built.FindIndex(u => u.Type == req.Item3 && u.X == req.Item2.X && u.Y == req.Item2.Y);
            if (idx >= 0)
            {
                var ret = Tuple.Create(req.Item1, built[idx]);
                built.RemoveAt(idx);
                return ret;
            }
            else return null;
        })
        .Where(b => b != null)
        .ToArray();

        this.condition.income = input.currentResources + builtRequests.Sum(su => Unit.CostArray[(int)su.Item2.Type]) - (this.condition.prevInput == null ? 0 : this.condition.prevInput.currentResources);

        var converted = this.strategies.SelectMany(strategy => strategy.ConvertList).ToLookup(conv => conv.Item2.Name, conv => conv.Item1);

        var actions = strategies.AsParallel().SelectMany(strategy => strategy.GetActions(condition, builtRequests.Where(br => br.Item1 == strategy).Select(br => br.Item2).ToList(), converted[strategy.GetType().Name].ToList(), killed)).ToDictionary(kv => kv.Key, kv => kv.Value);

        var buildRequestGroups = strategies.SelectMany(strategy =>
            strategy.RequestForBuilding.Select(b => Tuple.Create(strategy, b.Item1, b.Item2, b.Item3)))
            .ToLookup(b => b.Item2);
        this.requestsForBuilding =
            buildRequestGroups.Where(g => g.Key == null).SelectMany(g => g.ToArray())
            .Concat(buildRequestGroups.Where(g => g.Key != null).Select(g => g.OrderByDescending(b => b.Item4).First()))
            .OrderByDescending(g => g.Item4)
            .ToList();

        var currentRes = condition.currentInput.currentResources;
        foreach (var b in this.requestsForBuilding)
        {
            var cost = Unit.CostArray[(int)b.Item3];
            if (currentRes < cost) break;
            if (b.Item2 != null) actions[b.Item2] = (char)('0' + (int)b.Item3);
            currentRes -= cost;
        }

        return actions;
    }
}

enum UnitType
{
    Worker = 0,
    Knight = 1,
    Fighter = 2,
    Assassin = 3,
    Castle = 4,
    Village = 5,
    Base = 6
}

class Point : IComparable<Point>
{
    public readonly int X;
    public readonly int Y;
    protected Point(int y, int x)
    {
        this.X = x;
        this.Y = y;
    }

    static Point[][] pointCache;
    static Point()
    {
        Point.pointCache = Enumerable.Range(0, 100).Select(y => Enumerable.Range(0, 100).Select(x => new Point(y, x)).ToArray()).ToArray();
    }

    public static Point Get(int y, int x)
    {
        return Point.pointCache[y][x];
    }

    #region IComparable implementation
    public int CompareTo(Point other)
    {
        if (other == null) return 1;
        else return this.GetHashCode() - other.GetHashCode();
    }
    #endregion

    public override int GetHashCode()
    {
        return 10000 * this.Y + this.X;
    }

    public int DistanceFrom(Point point)
    {
        int temp;
        this.DirectionFrom(point.Y, point.X, out temp);
        return temp;
    }

    public char DirectionFrom(int y, int x, out int remainingDistance)
    {
        int dy = this.Y - y;
        int dx = this.X - x;
        remainingDistance = Math.Abs(dy) + Math.Abs(dx);
        if (remainingDistance == 0) return ' ';
        bool moveUD;
        if (dx == 0) moveUD = true;
        else if (dy == 0) moveUD = false;
        else moveUD = Math.Abs(dy) < Math.Abs(dx);

        if (moveUD) return dy > 0 ? 'D' : 'U';
        else return dx > 0 ? 'R' : 'L';
    }

    public char DirectionFrom(Point point, out int remainingDistance)
    {
        return DirectionFrom(point.Y, point.X, out remainingDistance);
    }

    public char DirectionFrom(int y, int x)
    {
        int temp;
        return DirectionFrom(y, x, out temp);
    }
    public char DirectionFrom(Point point)
    {
        return DirectionFrom(point.Y, point.X);
    }

    public IEnumerable<Point> GetPointsByDistance(int distance)
    {
        int maxdx = Math.Min(99 - this.X, distance);
        int mindx = Math.Max(-this.X, -distance);
        for (int dx = mindx; dx <= maxdx; dx++)
        {
            int x = this.X + dx;
            int y = this.Y - (distance - Math.Abs(dx));
            if (y < 0 || 99 < y) continue;
            yield return Point.Get(y, x);
        }
        maxdx = Math.Min(maxdx, distance - 1);
        mindx = Math.Max(mindx, -distance + 1);
        for (int dx = maxdx; dx >= mindx; dx--)
        {
            int x = this.X + dx;
            int y = this.Y + (distance - Math.Abs(dx));
            if (y < 0 || 99 < y) continue;
            yield return Point.Get(y, x);
        }
    }

    public IEnumerable<Point> GetPointsByDistanceInside(int distance)
    {
        return new Point[] { this }.Concat(Enumerable.Range(1, distance).SelectMany(d => this.GetPointsByDistance(d).OrderBy(p => Math.Abs(Math.Abs(this.X - p.X) - Math.Abs(this.Y - p.Y)))));
    }

    public Point GetNextPoint(char direction)
    {
        return Point.Get(Math.Max(0, Math.Min(99, this.Y + (direction == 'D' ? 1 : direction == 'U' ? -1 : 0))), Math.Max(0, Math.Min(99, this.X + (direction == 'R' ? 1 : direction == 'L' ? -1 : 0))));
    }

    public Point Offset(int dy, int dx)
    {
        return Point.Get(Math.Max(0, Math.Min(99, this.Y + dy)), Math.Max(0, Math.Min(99, this.X + dx)));
    }
}

class Unit : IEquatable<Unit>, IComparable<Unit>
{
    public readonly int Id;
    public int X { get { return this.point.X; } }
    public int Y { get { return this.point.Y; } }
    protected Point point;
    public Point Point
    {
        set { if (this.point != value) this.PrevPoint = this.Point; this.point = value; }
        get { return this.point; }
    }
    public Point PrevPoint;
    public Point TargetPoint;
    public int HP;
    public int HPDiff;
    public readonly UnitType Type;
    public Unit(int id, int y, int x, int hp, UnitType type)
    {
        this.Id = id;
        this.Point = Point.Get(y, x);
        this.HP = hp;
        this.HPDiff = 0;
        this.Type = type;
        this.PrevPoint = this.Point;
    }

    public static Unit FromString(string input, bool is1P)
    {
        var vals = input.Split(' ').Select(int.Parse).ToArray();
        return new Unit(vals[0], is1P ? vals[1] : 99 - vals[1], is1P ? vals[2] : 99 - vals[2], vals[3], (UnitType)vals[4]);
    }

    public static readonly int[][] DamageArrayAttack = new int[][]{
		new int[]{100,100,100,100,100,100,100},
		new int[]{100,500,200,200,200,200,200},
		new int[]{500,1600,500,200,200,200,200},
		new int[]{1000,500,1000,500,200,200,200},
		new int[]{100,100,100,100,100,100,100},
		new int[]{100,100,100,100,100,100,100},
		new int[]{100,100,100,100,100,100,100},
	};

    public static readonly int[] CostArray = new int[] { 40, 20, 40, 60, 99999999, 100, 500 };

    public bool Equals(Unit other)
    {
        return this.Id == other.Id;
    }
    public override int GetHashCode()
    {
        return this.Id;
    }

    public int CompareTo(Unit other)
    {
        return this.Id - other.Id;
    }
}

class Input
{
    public int RemainingTime;
    public int StageNo;
    public int Turn;
    public int currentResources;
    public List<Unit> Units;
    public List<Unit> EnemyUnits;
    public List<Point> Resources;
    public bool is1P;

    private Input() { }

    public static Input FromConsole()
    {
        var ret = new Input();
        ret.RemainingTime = ReadInt();
        ret.StageNo = ReadInt();
        ret.Turn = ReadInt();
        ret.currentResources = ReadInt();
        var unitCount = ReadInt();
        var castle = Unit.FromString(Console.ReadLine(), true);
        var is1P = castle.Point.DistanceFrom(Point.Get(0, 0)) < 50;
        ret.is1P = is1P;
        if (!is1P) castle.Point = Point.Get(99 - castle.Y, 99 - castle.X);
        ret.Units = new List<Unit>(new Unit[] { castle });
        ret.Units.AddRange(Enumerable.Range(0, unitCount - 1).Select(i => Unit.FromString(Console.ReadLine(), is1P)));
        ret.EnemyUnits = Enumerable.Range(0, ReadInt()).Select(i => Unit.FromString(Console.ReadLine(), is1P)).ToList();
        ret.Resources = Enumerable.Range(0, ReadInt()).Select(i => Console.ReadLine().Split(' ').Select(int.Parse).ToArray()).Select(yx => Point.Get(is1P ? yx[0] : 99 - yx[0], is1P ? yx[1] : 99 - yx[1])).ToList();
        Console.ReadLine();
        return ret;
    }

    private static int ReadInt()
    {
        try { return int.Parse(Console.ReadLine()); }
        catch { return int.Parse(Console.ReadLine()); }
    }
}

class Utils
{
    public static IEnumerable<Unit> GetUnitsInside(List<Unit>[][] map, Point point, int distance)
    {
        return point.GetPointsByDistanceInside(distance).SelectMany(p => map[p.Y][p.X]);
    }

    public static int[][][] GetInfluenceMap(List<Unit>[][] map, int maxDistance, Func<Unit, List<Unit>, int, UnitType, int> selector)
    {
        var ret = Enumerable.Range(0, 100).Select(y => Enumerable.Range(0, 100).Select(x => new int[Unit.DamageArrayAttack.Length]).ToArray()).ToArray();
        for (int y = 0; y < 100; y++)
        {
            for (int x = 0; x < 100; x++)
            {
                for (int distance = 0; distance <= maxDistance; distance++)
                {
                    var d = distance;
                    var units = map[y][x];
                    foreach (var u in units)
                    {
                        for (int i = 0; i < Unit.DamageArrayAttack.Length; i++)
                        {
                            int val = selector(u, units, d, (UnitType)i);
                            foreach (var p in Point.Get(y, x).GetPointsByDistance(distance)) ret[p.Y][p.X][i] += val;
                        }
                    }

                }
            }
        }
        return ret;
    }

    public static Point GetAveragePoint(IEnumerable<Point> points)
    {
        return Point.Get((int)points.Average(p => p.Y), (int)points.Average(p => p.X));
    }
}
